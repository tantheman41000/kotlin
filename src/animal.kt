abstract class animal(var numOfLeg: Int, var food: String) {
    open fun getSound(): String {
        return "sound"
    }
    open fun getDetails():String{
        return "$numOfLeg $food "
    }
}

class Dog(numOfLeg: Int, food: String, var name:String): animal(numOfLeg, food){
    override fun getSound(): String{
        return "bog bog bog"
    }
    override fun getDetails():String{
        return "this dog has $numOfLeg legs, it eats $food,name $name and sound like ${getSound()}"
    }
}

class Lion(numOfLeg: Int, food: String) : animal(numOfLeg,food){
    override fun getSound(): String {
        return "rawwww"
    }
    override fun getDetails():String{
        return "this lion has $numOfLeg, it eats $food, sound like ${getSound()}"
    }
}

fun main(args: Array<String>) {
    val beagle:animal = Dog(4,"can meat","Icarus")
    val L1:animal = Lion(4,"raw meat")
    println(beagle.getDetails())
    println(L1.getDetails())
}