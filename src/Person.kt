


class Student (name: String, surname: String,gpa: Double, var department: String): Person(name,surname,gpa){
    override fun getDetails(): String {
            return super.getDetails() + " and study in $department"
        }
    override fun goodBoy():Boolean {
        return gpa > 2.0
    }




}


abstract class Person (var name:String, var surname:String, var gpa:Double){
    constructor(name: String,surname: String) : this(name,surname, 0.0){}

    abstract fun goodBoy():Boolean
    open fun getDetails():String{
        return "$name $surname has score $gpa"
    }

}

    fun main(args: Array<String>){
        val nubb: Person = Student("noom","nool",2.66,"software engineering")


        println(nubb.getDetails())

    }
